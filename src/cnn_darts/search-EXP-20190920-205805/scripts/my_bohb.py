import numpy
import time
import argparse
import logging
import ConfigSpace as CS
import ConfigSpace.hyperparameters as CSH
from hpbandster.core.worker import Worker
import hpbandster.core.nameserver as hpns
import hpbandster.core.result as hpres
from hpbandster.optimizers.bohb import BOHB
from train import train, main
import torch
from utils import *
import torchvision.transforms as transforms

class MyWorker(Worker):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


    def compute(self, config, budget=20, **kwargs):

        settings = {
            "data": '../data',
            "batch_size": int(config['batch_size']),
            "learning_rate": float(config['learning_rate']),
            "weight_decay": float(config['weight_decay']),
            "report_freq": 50,
            "gpu": 0,
            "epochs": int(budget),
            "init_channels": int(config['init_channels']),
            "layers": int(config['layers']),
            "model_path": 'saved_models',
            "cutout": False,
            "cutout_length": 16,
            "drop_path_prob": 0.2,
            "save": 'EXP',
            "seed": 0,
            "arch": 'DARTS',
            "grad_clip": 5
        }
        print('xxx' * 50)
        print(settings)
        print('xxx' * 50)
        model_optimizer = optimizers_dict[config['model_optimizer']]
        if config['model_optimizer'] == 'adam':
            optimizer_params = True if config['amsgrad'] == 'True' else False
        elif config['model_optimizer'] == 'sgd':
            optimizer_params = config['momentum']
        else:
            optimizer_params = None


        valid_acc, valid_obj = main(settings, model_optimizer, optimizer_params, dataset='K49')
        return ({
            'loss': 100 - float(valid_acc),
            'info': {'valid_acc':valid_acc}
            })

    @staticmethod
    def get_configspace():

        config_space = CS.ConfigurationSpace()

        ### OPTIMIZER HYPERPARAMETERS ###
        learning_rate = CSH.UniformFloatHyperparameter('learning_rate', lower=1e-4, upper=1e-1, default_value='1e-2', log=True)
        model_optimizer = CSH.CategoricalHyperparameter('model_optimizer', choices=['adam', 'adad', 'sgd'], default_value='sgd')
        amsgrad = CSH.CategoricalHyperparameter('amsgrad', choices=['True', 'False'], default_value='False')
        sgd_momentum = CSH.UniformFloatHyperparameter('momentum', lower=0.0, upper=0.99, default_value=0.9, log=False)
        weight_decay = CSH.CategoricalHyperparameter('weight_decay', choices=['0.0', '0.1', '0.01'], default_value='0.0')
        config_space.add_hyperparameters([learning_rate, model_optimizer, amsgrad, sgd_momentum, weight_decay])
        amsgrad_cond = CS.EqualsCondition(amsgrad, model_optimizer, 'adam')
        sgd_momentum_cond = CS.EqualsCondition(sgd_momentum, model_optimizer, 'sgd')
        config_space.add_conditions([amsgrad_cond, sgd_momentum_cond])

        batch_size = CSH.CategoricalHyperparameter('batch_size', choices=['32', '64', '128'], default_value='64')
        layers = CSH.UniformIntegerHyperparameter('layers', lower=1, upper=5, default_value=3, log=False)
        init_channels = CSH.CategoricalHyperparameter('init_channels', choices=['8', '16', '32'], default_value='16')
        config_space.add_hyperparameters([batch_size, layers, init_channels])

        return(config_space)



if __name__ == "__main__":
    cmdline_parser = argparse.ArgumentParser('AutoML SS19 final project')
    cmdline_parser.add_argument("-d", "--dataset",
                                default='KMNIST',
                                choices=['KMNIST', 'K49'],
                                help='Which dataset to use',
                                type=str.upper)
    cmdline_parser.add_argument("-s", "--surrogate_model",
                                default='RF',
                                choices=['KDE', 'RF', 'DNGO'],
                                help='kde, random forrest or dngo',
                                type=str.upper)
    cmdline_parser.add_argument('-i', "--n_iterations",
                                type=int,
                                default=1,
                                help='Number of BOHB iterations that will be run')
    cmdline_parser.add_argument('-b', "--min_budget",
                                type=int,
                                default=1,
                                help='Minimum BOHB budget')
    cmdline_parser.add_argument('-B', "--max_budget",
                                type=int,
                                default=2,
                                help='Maximum BOHB budget')
    cmdline_parser.add_argument('-e', "--eta",
                                type=int,
                                default=2,
                                help='The fraction of configurations passed to next budget by BOHB')
    cmdline_parser.add_argument('-o', "--out_dir",
                                type=str,
                                default='bohb/',
                                help='The output directory to write all results')
    cmdline_parser.add_argument('-r', "--run_id",
                                type=str,
                                default='bohb',
                                help='ID')

    cmdline_parser.add_argument('-v', '--verbose',
                                default='INFO',
                                choices=['INFO', 'DEBUG'],
                                help='verbosity')
    args, kwargs = cmdline_parser.parse_known_args()
    log_lvl = logging.INFO if args.verbose == 'INFO' else logging.DEBUG
    logging.basicConfig(level=log_lvl)

    start_time = time.time()

    dataset = args.dataset

    host = '127.0.0.1'
    NS = hpns.NameServer(run_id=args.run_id, host=host, port=0)
    ns_host, ns_port = NS.start()
    w = MyWorker(run_id=args.run_id, host=host, nameserver=ns_host, nameserver_port=ns_port, timeout=120)
    w.run(background=True)
    result_logger = hpres.json_result_logger(directory=args.out_dir, overwrite=True)

    bohb = BOHB(  configspace = w.get_configspace(),
                  run_id = args.run_id,
                  host=host,
                  nameserver=ns_host,
                  nameserver_port=ns_port,
                  result_logger=result_logger,
                  min_budget=args.min_budget,
                  max_budget=args.max_budget,
                  eta = args.eta,
                  random_fraction=0.1,
                  num_samples=4)


    res = bohb.run(n_iterations=args.n_iterations)
    bohb.shutdown(shutdown_workers=True)
    NS.shutdown()

    print("took: ", time.time() - start_time)

    id2config = res.get_id2config_mapping()
    incumbent = res.get_incumbent_id()

    print('xxx' * 50)
    print('Best found configuration:', id2config[incumbent]['config'])
    print(res.get_runs_by_id(incumbent)[-1]['info'])
    print('xxx' * 50)
