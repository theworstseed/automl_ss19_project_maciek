import os
import sys
import time
import glob
import numpy as np
import torch
import utils
import logging
import argparse
import torch.nn as nn
import genotypes
import torch.utils
import torchvision.datasets as dset
import torchvision.transforms as transforms
import torch.backends.cudnn as cudnn

from torch.autograd import Variable
from model import NetworkKMNIST as Network
from datasets import KMNIST, K49
from utils import *

def submain(settings, model_optimizer, optimizer_params, dataset='KMNIST'):

  temp = 'eval-{}-{}'.format(settings['save'], time.strftime("%Y%m%d-%H%M%S"))
  # utils.create_exp_dir(temp, scripts_to_save=glob.glob('*.py'))

  log_format = '%(asctime)s %(message)s'
  logging.basicConfig(stream=sys.stdout, level=logging.INFO,
    format=log_format, datefmt='%m/%d %I:%M:%S %p')
  # fh = logging.FileHandler(os.path.join(temp, 'log.txt'))
  # fh.setFormatter(logging.Formatter(log_format))
  # logging.getLogger().addHandler(fh)


  if (dataset == 'KMNIST'):
    n_classes = 10
  elif (dataset == 'K49'):
    n_classes = 49
  else:
    n_classes = 10

  if not torch.cuda.is_available():
    logging.info('no gpu device available')
    sys.exit(1)

  np.random.seed(settings['seed'])
  torch.cuda.set_device(settings['gpu'])
  cudnn.benchmark = True
  torch.manual_seed(settings['seed'])
  cudnn.enabled=True
  torch.cuda.manual_seed(settings['seed'])
  logging.info('gpu device = %d' % settings['gpu'])
  # logging.info("args = %s", args)

  genotype = eval("genotypes.%s" % settings['arch'])
  model = Network(settings['init_channels'], n_classes, settings['layers'], genotype)
  model = model.cuda()

  logging.info("param size = %fMB", utils.count_parameters_in_MB(model))

  criterion = nn.CrossEntropyLoss()
  criterion = criterion.cuda()
  model_optimizer = optimizers_dict[model_optimizer]
  if model_optimizer == torch.optim.Adam:
      optimizer = model_optimizer(model.parameters(), lr=settings['learning_rate'], weight_decay=settings['weight_decay'], amsgrad=optimizer_params)
  elif model_optimizer == torch.optim.SGD:
      optimizer = model_optimizer(model.parameters(), lr=settings['learning_rate'], weight_decay=settings['weight_decay'], momentum=optimizer_params)
  else:
      optimizer = model_optimizer(model.parameters(), lr=settings['learning_rate'], weight_decay=settings['weight_decay'])


  # optimizer = torch.optim.SGD(
  #     model.parameters(),
  #     settings['learning_rate'],
  #     momentum=settings['momentum'],
  #     weight_decay=settings['weight_decay']
  #     )

  # train_transform, valid_transform = utils._data_transforms_cifar10(args)
  # train_data = dset.CIFAR10(root=args.data, train=True, download=True, transform=train_transform)
  # valid_data = dset.CIFAR10(root=args.data, train=False, download=True, transform=valid_transform)
  #
  # train_queue = torch.utils.data.DataLoader(
  #     train_data, batch_size=settings['batch_size'], shuffle=True, pin_memory=True, num_workers=2)
  #
  # valid_queue = torch.utils.data.DataLoader(
  #     valid_data, batch_size=settings['batch_size'], shuffle=False, pin_memory=True, num_workers=2)
  data_dir='../../data'
  data_augmentations = None
  if data_augmentations is None:
      # You can add any preprocessing/data augmentation you want here
      data_augmentations = transforms.ToTensor()

  if (dataset == 'KMNIST'):
    train_data = KMNIST(data_dir, True, data_augmentations)
  elif (dataset == 'K49'):
    train_data = K49(data_dir, True, data_augmentations)
  else:
    train_data = KMNIST(data_dir, True, data_augmentations)
  # test_dataset = KMNIST(data_dir, False, data_augmentations)
  # train_loader = DataLoader(dataset=train_dataset,
  #                           batch_size=batch_size,
  #                           shuffle=True)

  num_train = len(train_data)
  indices = list(range(num_train))
  split = int(np.floor(.7 * num_train))

  train_queue = torch.utils.data.DataLoader(
      train_data, batch_size=settings['batch_size'],
      sampler=torch.utils.data.sampler.SubsetRandomSampler(indices[:split]),
      pin_memory=True, num_workers=2)

  valid_queue = torch.utils.data.DataLoader(
      train_data, batch_size=settings['batch_size'],
      sampler=torch.utils.data.sampler.SubsetRandomSampler(indices[split:num_train]),
      pin_memory=True, num_workers=2)

  train_acc_list = []
  valid_acc_list = []
  scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, float(settings['epochs']))

  for epoch in range(settings['epochs']):
    scheduler.step()
    logging.info('epoch %d lr %e', epoch, scheduler.get_lr()[0])
    model.drop_path_prob = settings['drop_path_prob'] * epoch / settings['epochs']

    train_acc, train_obj = train(train_queue, model, criterion, optimizer, settings)
    train_acc_list.append(train_acc)
    logging.info('train_acc %f', train_acc)

    valid_acc, valid_obj = infer(valid_queue, model, criterion)
    valid_acc_list.append(valid_acc)
    logging.info('valid_acc %f', valid_acc)

    # utils.save(model, os.path.join(temp, 'weights.pt'))

  return model, criterion, train_acc_list, valid_acc_list


def train(train_queue, model, criterion, optimizer, settings):
  objs = utils.AvgrageMeter()
  top1 = utils.AvgrageMeter()
  top5 = utils.AvgrageMeter()
  model.train()

  for step, (input, target) in enumerate(train_queue):
    input = Variable(input).cuda()
    target = Variable(target).cuda()

    optimizer.zero_grad()
    logits = model(input)
    loss = criterion(logits, target)

    loss.backward()
    nn.utils.clip_grad_norm(model.parameters(), settings['grad_clip'])
    optimizer.step()

    prec1, prec5 = utils.accuracy(logits, target, topk=(1, 5))
    n = input.size(0)
    objs.update(loss.data.item(), n)
    top1.update(prec1.data.item(), n)
    top5.update(prec5.data.item(), n)

    if step % 50 == 0:
      logging.info('train %03d %e %f %f', step, objs.avg, top1.avg, top5.avg)

  return top1.avg, objs.avg


def infer(valid_queue, model, criterion):
  objs = utils.AvgrageMeter()
  top1 = utils.AvgrageMeter()
  top5 = utils.AvgrageMeter()
  model.eval()

  for step, (input, target) in enumerate(valid_queue):
    input = Variable(input, volatile=True).cuda()
    target = Variable(target, volatile=True).cuda()

    logits = model(input)
    loss = criterion(logits, target)

    prec1, prec5 = utils.accuracy(logits, target, topk=(1, 5))
    n = input.size(0)
    objs.update(loss.data.item(), n)
    top1.update(prec1.data.item(), n)
    top5.update(prec5.data.item(), n)

    if step % 50 == 0:
      logging.info('valid %03d %e %f %f', step, objs.avg, top1.avg, top5.avg)

  return top1.avg, objs.avg
