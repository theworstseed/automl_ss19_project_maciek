import os
import sys
import glob
import numpy as np
import torch
import utils
import logging
import argparse
import torch.nn as nn
import genotypes
import torch.utils
import torchvision.datasets as dset
import torch.backends.cudnn as cudnn
from train import submain

from torch.autograd import Variable
from model import NetworkKMNIST as Network
from datasets import KMNIST, K49
import torchvision.transforms as transforms
from utils import *

parser = argparse.ArgumentParser("cifar")
parser.add_argument('--data', type=str, default='../../data', help='location of the data corpus')
parser.add_argument('--batch_size', type=int, default=256, help='batch size')
parser.add_argument('--report_freq', type=float, default=50, help='report frequency')
parser.add_argument('--gpu', type=int, default=0, help='gpu device id')
parser.add_argument('--init_channels', type=int, default=36, help='num of init channels')
parser.add_argument('--layers', type=int, default=4, help='total number of layers')
parser.add_argument('--model_path', type=str, default='EXP/model.pt', help='path of pretrained model')
parser.add_argument('--cutout', action='store_true', default=False, help='use cutout')
parser.add_argument('--cutout_length', type=int, default=16, help='cutout length')
parser.add_argument('--drop_path_prob', type=float, default=0.2, help='drop path probability')
parser.add_argument('--seed', type=int, default=0, help='random seed')
parser.add_argument('--arch', type=str, default='MY_DARTS', help='which architecture to use')
args = parser.parse_args()

log_format = '%(asctime)s %(message)s'
logging.basicConfig(stream=sys.stdout, level=logging.INFO,
        format=log_format, datefmt='%m/%d %I:%M:%S %p')

n_classes = 49

settings = {
        "data": '../data',
        "batch_size": 256,
        "learning_rate": float(0.005153975854692469),
        "weight_decay": 0.0,
        "report_freq": 50,
        "gpu": 0,
        "epochs": 20,
        "init_channels": 16,
        "layers": 3,
        "model_path": 'saved_models',
        "cutout": False,
        "cutout_length": 16,
        "drop_path_prob": 0.2,
        "save": 'EXP',
        "seed": 0,
        "arch": 'DARTS',
        "grad_clip": 5
}


def main():

    model, criterion, train_acc_list, valid_acc_list = submain(settings, model_optimizer='adam', optimizer_params=False, dataset='K49')
    # if not torch.cuda.is_available():
    #     logging.info('no gpu device available')
    #     sys.exit(1)
    #
    # np.random.seed(args.seed)
    # torch.cuda.set_device(args.gpu)
    # cudnn.benchmark = True
    # torch.manual_seed(args.seed)
    # cudnn.enabled=True
    # torch.cuda.manual_seed(args.seed)
    # logging.info('gpu device = %d' % args.gpu)
    # logging.info("args = %s", args)
    #
    # genotype = eval("genotypes.%s" % args.arch)
    # model = Network(args.init_channels, n_classes, args.layers, genotype)
    # model = model.cuda()
    # utils.load(model, args.model_path)
    #
    # logging.info("param size = %fMB", utils.count_parameters_in_MB(model))
    #
    # criterion = nn.CrossEntropyLoss()
    # criterion = criterion.cuda()

    # _, test_transform = utils._data_transforms_cifar10(args)
    # test_data = dset.CIFAR10(root=args.data, train=False, download=True, transform=test_transform)
    dataset='K49'
    data_dir='../../data'
    data_augmentations = None
    if data_augmentations is None:
        # You can add any preprocessing/data augmentation you want here
            data_augmentations = transforms.ToTensor()
    if (dataset == 'K49'):
        test_dataset = K49(data_dir, False, data_augmentations)
    else:
        test_dataset = KMNIST(data_dir, False, data_augmentations)

    test_queue = torch.utils.data.DataLoader(
            test_dataset, batch_size=args.batch_size, shuffle=False, pin_memory=True, num_workers=2)

    model.drop_path_prob = args.drop_path_prob
    test_acc, test_obj = infer(test_queue, model, criterion)
    logging.info('test_acc %f', test_acc)
    print('train_acc_list: ', train_acc_list)
    print('valid_acc_list: ', valid_acc_list)
    print('test_acc: ', test_acc)


def infer(test_queue, model, criterion):
    objs = utils.AvgrageMeter()
    top1 = utils.AvgrageMeter()
    top5 = utils.AvgrageMeter()
    model.eval()

    for step, (input, target) in enumerate(test_queue):
        input = Variable(input, volatile=True).cuda()
        target = Variable(target, volatile=True).cuda()

        logits = model(input)
        loss = criterion(logits, target)

        prec1, prec5 = utils.accuracy(logits, target, topk=(1, 5))
        n = input.size(0)
        objs.update(loss.item(), n)
        top1.update(prec1.item(), n)
        top5.update(prec5.item(), n)

        if step % args.report_freq == 0:
            logging.info('test %03d %e %f %f', step, objs.avg, top1.avg, top5.avg)

    return top1.avg, objs.avg


if __name__ == '__main__':
    main()
