import numpy
import time
import argparse
import logging
import ConfigSpace as CS
import ConfigSpace.hyperparameters as CSH
from hpbandster.core.worker import Worker
import hpbandster.core.nameserver as hpns
import hpbandster.core.result as hpres
from optimizers.bohb import BOHB as BOHB
from optimizers.lcnet import LCNet as LCNet
from main import train
import torch
from utils import *

class MyWorker(Worker):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


    def compute(self, config, budget=20, **kwargs):

        num_epochs = int(budget)
        batch_size = int(config['batch_size'])
        learning_rate = config['learning_rate']
        weight_decay = float(config['weight_decay'])

        training_loss = training_loss_dict['cross_entropy']

        model_optimizer = optimizers_dict[config['model_optimizer']]
        if config['model_optimizer'] == 'adam':
            optimizer_params = True if config['amsgrad'] == 'True' else False
        elif config['model_optimizer'] == 'sgd':
            optimizer_params = config['momentum']
        else:
            optimizer_params = None

        print('***' * 40)
        print(config)
        print('***' * 40)

        train_score, train_loss, test_score, learning_curve = train(
                                                                    config=config,
                                                                    dataset=dataset,
                                                                    data_dir=data_dir,
                                                                    num_epochs=num_epochs,
                                                                    batch_size=batch_size,
                                                                    learning_rate=learning_rate,
                                                                    weight_decay=weight_decay,
                                                                    train_criterion=training_loss,
                                                                    model_optimizer=model_optimizer,
                                                                    optimizer_params=optimizer_params,
                                                                    data_augmentations=data_augmentation, ## TODO?
                                                                    save_model_str='models/model_'
                                                                    )
        return ({
            'loss': 100 - float(test_score),
            'info': {'train_score':float(train_score),
                    'train_loss':float(train_loss),
                    'learning_curve':learning_curve}
            })

    @staticmethod
    def get_configspace():

        config_space = CS.ConfigurationSpace()

        ### OPTIMIZER HYPERPARAMETERS ###
        batch = CSH.CategoricalHyperparameter('batch_size', choices=['32', '64', '128', '256', '512'], default_value='64')
        learning_rate = CSH.UniformFloatHyperparameter('learning_rate', lower=1e-4, upper=1e-1, default_value='1e-2', log=True)
        model_optimizer = CSH.CategoricalHyperparameter('model_optimizer', choices=['adam', 'adad', 'sgd'], default_value='sgd')
        amsgrad = CSH.CategoricalHyperparameter('amsgrad', choices=['True', 'False'], default_value='False')
        sgd_momentum = CSH.UniformFloatHyperparameter('momentum', lower=0.0, upper=0.99, default_value=0.9, log=False)
        weight_decay = CSH.CategoricalHyperparameter('weight_decay', choices=['0.0', '0.1', '0.01'], default_value='0.0')
        config_space.add_hyperparameters([batch, learning_rate, model_optimizer, amsgrad, sgd_momentum, weight_decay])
        amsgrad_cond = CS.EqualsCondition(amsgrad, model_optimizer, 'adam')
        sgd_momentum_cond = CS.EqualsCondition(sgd_momentum, model_optimizer, 'sgd')
        config_space.add_conditions([amsgrad_cond, sgd_momentum_cond])

        ### ARCHITECTURE ###
        n_layers = CSH.UniformIntegerHyperparameter('n_layers', lower=1, upper=3, default_value=1, log=False)
        n_conv_layers = CSH.UniformIntegerHyperparameter('n_conv_layers', lower=1, upper=3, default_value=3, log=False)
        activation = CSH.CategoricalHyperparameter('activation', choices=['relu', 'sigmoid', 'tanh'], default_value='relu')
        dropout_rate = CSH.CategoricalHyperparameter('dropout_rate', choices=['0.1', '0.2', '0.3'], default_value='0.2')

        config_space.add_hyperparameters([n_layers, n_conv_layers, activation, dropout_rate])

        ### LAYER 1 ###
        kernel_size_1 = CSH.CategoricalHyperparameter('kernel_size_1', choices=['3', '5', '7'], default_value='5')
        stride_1 = CSH.UniformIntegerHyperparameter('stride_1', lower=1, upper=3, default_value=2)
        # padding_1 = CSH.UniformIntegerHyperparameter('padding_1', lower=0, upper=3, default_value=1)
        dropout_1 = CSH.CategoricalHyperparameter('dropout_1', choices=['True', 'False'], default_value='False')
        batch_norm_1 = CSH.CategoricalHyperparameter('batch_norm_1', choices=['True', 'False'], default_value='False')
        out_channels_1 = CSH.CategoricalHyperparameter('out_channels_1', choices=['4', '8', '16'], default_value='4')
        maxpool_1 = CSH.CategoricalHyperparameter('maxpool_1', choices=['True', 'False'], default_value='False')
        maxpool_kernel_size_1 = CSH.CategoricalHyperparameter('maxpool_kernel_size_1', choices=['2', '3', '4', '5', '6'], default_value='2')
        maxpool_stride_1 = CSH.UniformIntegerHyperparameter('maxpool_stride_1', lower=1, upper=3, default_value=2)
        config_space.add_hyperparameters([kernel_size_1, stride_1, dropout_1, batch_norm_1, out_channels_1, maxpool_1, maxpool_kernel_size_1, maxpool_stride_1])

        use_maxpool_kernel_1 = CS.EqualsCondition(maxpool_kernel_size_1, maxpool_1, 'True')
        use_maxpool_stride_1 = CS.EqualsCondition(maxpool_stride_1, maxpool_1, 'True')
        config_space.add_conditions([use_maxpool_kernel_1, use_maxpool_stride_1])

        # padding_1_forbidden_1 = CS.ForbiddenAndConjunction(
        #                                             CS.ForbiddenEqualsClause(kernel_size_1, '3'),
        #                                             CS.ForbiddenInClause(padding_1, [2,3])
        # )
        # padding_1_forbidden_2 = CS.ForbiddenAndConjunction(
        #                                             CS.ForbiddenEqualsClause(kernel_size_1, '5'),
        #                                             CS.ForbiddenInClause(padding_1, [3])
        # )
        # config_space.add_forbidden_clauses([padding_1_forbidden_1, padding_1_forbidden_2])

        ### LAYER 2 ###
        kernel_size_2 = CSH.CategoricalHyperparameter('kernel_size_2', choices=['2', '3', '5', '7'], default_value='3')
        stride_2 = CSH.UniformIntegerHyperparameter('stride_2', lower=1, upper=3, default_value=2)
        # padding_2 = CSH.UniformIntegerHyperparameter('padding_2', lower=0, upper=3, default_value=1)
        dropout_2 = CSH.CategoricalHyperparameter('dropout_2', choices=['True', 'False'], default_value='False')
        batch_norm_2 = CSH.CategoricalHyperparameter('batch_norm_2', choices=['True', 'False'], default_value='False')
        out_channels_2 = CSH.CategoricalHyperparameter('out_channels_2', choices=['8', '16', '32'], default_value='8')
        maxpool_2 = CSH.CategoricalHyperparameter('maxpool_2', choices=['True', 'False'], default_value='False')
        maxpool_kernel_size_2 = CSH.CategoricalHyperparameter('maxpool_kernel_size_2', choices=['2', '3', '4', '5', '6'], default_value='2')
        maxpool_stride_2 = CSH.UniformIntegerHyperparameter('maxpool_stride_2', lower=1, upper=3, default_value=2)
        config_space.add_hyperparameters([kernel_size_2, stride_2, dropout_2, batch_norm_2, out_channels_2, maxpool_2, maxpool_kernel_size_2, maxpool_stride_2])

        # padding_2_forbidden_1 = CS.ForbiddenAndConjunction(
        #                                             CS.ForbiddenEqualsClause(kernel_size_2, '2'),
        #                                             CS.ForbiddenInClause(padding_2, [2,3])
        # )
        # padding_2_forbidden_2 = CS.ForbiddenAndConjunction(
        #                                             CS.ForbiddenEqualsClause(kernel_size_2, '3'),
        #                                             CS.ForbiddenInClause(padding_2, [2,3])
        # )
        # padding_2_forbidden_3 = CS.ForbiddenAndConjunction(
        #                                             CS.ForbiddenEqualsClause(kernel_size_2, '5'),
        #                                             CS.ForbiddenInClause(padding_2, [3])
        # )
        # config_space.add_forbidden_clauses([padding_2_forbidden_1, padding_2_forbidden_2, padding_2_forbidden_3])

        ### DO WE EVEN USE IT? ### for n_conv_layers 1-3
        use_kernel_2 = CS.GreaterThanCondition(kernel_size_2, n_conv_layers, 1)
        use_stride_2 = CS.GreaterThanCondition(stride_2, n_conv_layers, 1)
        # use_padding_2 = CS.GreaterThanCondition(padding_2, n_conv_layers, 1)
        use_dropout_2 = CS.GreaterThanCondition(dropout_2, n_conv_layers, 1)
        use_batch_norm_2 = CS.GreaterThanCondition(batch_norm_2, n_conv_layers, 1)
        use_out_channel_2 = CS.GreaterThanCondition(out_channels_2, n_conv_layers, 1)
        use_maxpool_2 = CS.GreaterThanCondition(maxpool_2, n_conv_layers, 1)
        use_maxpool_kernel_2 = CS.AndConjunction(
                                                CS.GreaterThanCondition(maxpool_kernel_size_2, n_conv_layers, 1),
                                                CS.EqualsCondition(maxpool_kernel_size_2, maxpool_2, 'True')
                                                )
        use_maxpool_stride_2 = CS.AndConjunction(
                                                CS.GreaterThanCondition(maxpool_stride_2, n_conv_layers, 1),
                                                CS.EqualsCondition(maxpool_stride_2, maxpool_2, 'True')
                                                )
        config_space.add_conditions([use_kernel_2, use_stride_2, use_dropout_2, use_batch_norm_2, use_out_channel_2, use_maxpool_2, use_maxpool_kernel_2, use_maxpool_stride_2])

        # ### for n_conv_layers 2-3
        # use_maxpool_kernel_2 = CS.EqualsCondition(maxpool_kernel_size_2, maxpool_2, 'True')
        # use_maxpool_stride_2 = CS.EqualsCondition(maxpool_stride_2, maxpool_2, 'True')
        # config_space.add_conditions([use_maxpool_kernel_2, use_maxpool_stride_2])

        dim_forbbiden_1 = CS.ForbiddenAndConjunction(
                                                    CS.ForbiddenEqualsClause(n_conv_layers, 2),
                                                    CS.ForbiddenInClause(maxpool_kernel_size_1, ['4', '5', '6']),
                                                    CS.ForbiddenInClause(maxpool_kernel_size_2, ['4', '5', '6'])
        )
        dim_forbbiden_2 = CS.ForbiddenAndConjunction(
                                                    CS.ForbiddenInClause(maxpool_kernel_size_1, ['5', '6']),
                                                    CS.ForbiddenInClause(kernel_size_2, ['5', '7'])
        )
        config_space.add_forbidden_clauses([dim_forbbiden_1, dim_forbbiden_2])

        ### LAYER 3 ###
        kernel_size_3 = CSH.CategoricalHyperparameter('kernel_size_3', choices=['1', '3', '5', '7'], default_value='3')
        stride_3 = CSH.UniformIntegerHyperparameter('stride_3', lower=1, upper=3, default_value=2)
        # padding_3 = CSH.UniformIntegerHyperparameter('padding_3', lower=0, upper=3, default_value=1)
        dropout_3 = CSH.CategoricalHyperparameter('dropout_3', choices=['True', 'False'], default_value='False')
        batch_norm_3 = CSH.CategoricalHyperparameter('batch_norm_3', choices=['True', 'False'], default_value='False')
        out_channels_3 = CSH.CategoricalHyperparameter('out_channels_3', choices=['8', '16', '32', '64'], default_value='16')
        maxpool_3 = CSH.CategoricalHyperparameter('maxpool_3', choices=['True', 'False'], default_value='False')
        maxpool_kernel_size_3 = CSH.CategoricalHyperparameter('maxpool_kernel_size_3', choices=['2', '3', '4', '5', '6'], default_value='2')
        maxpool_stride_3 = CSH.UniformIntegerHyperparameter('maxpool_stride_3', lower=1, upper=3, default_value=2)
        config_space.add_hyperparameters([kernel_size_3, stride_3, dropout_3, batch_norm_3, out_channels_3, maxpool_3, maxpool_kernel_size_3, maxpool_stride_3])

        # padding_3_forbidden_1 = CS.ForbiddenAndConjunction(
        #                                             CS.ForbiddenEqualsClause(kernel_size_3, '1'),
        #                                             CS.ForbiddenInClause(padding_3, [1,2,3])
        # )
        # padding_3_forbidden_2 = CS.ForbiddenAndConjunction(
        #                                             CS.ForbiddenEqualsClause(kernel_size_3, '3'),
        #                                             CS.ForbiddenInClause(padding_3, [2,3])
        # )
        # padding_3_forbidden_3 = CS.ForbiddenAndConjunction(
        #                                             CS.ForbiddenEqualsClause(kernel_size_3, '5'),
        #                                             CS.ForbiddenInClause(padding_3, [3])
        # )
        # config_space.add_forbidden_clauses([padding_3_forbidden_1, padding_3_forbidden_2, padding_3_forbidden_3])

        ### DO WE EVEN USE IT? ###
        use_kernel_3 = CS.GreaterThanCondition(kernel_size_3, n_conv_layers, 2)
        use_stride_3 = CS.GreaterThanCondition(stride_3, n_conv_layers, 2)
        # use_padding_3 = CS.GreaterThanCondition(padding_3, n_conv_layers, 2)
        use_dropout_3 = CS.GreaterThanCondition(dropout_3, n_conv_layers, 2)
        use_batch_norm_3 = CS.GreaterThanCondition(batch_norm_3, n_conv_layers, 2)
        use_out_channel_3 = CS.GreaterThanCondition(out_channels_3, n_conv_layers, 2)
        use_maxpool_3 = CS.GreaterThanCondition(maxpool_3, n_conv_layers, 2)
        use_maxpool_kernel_3 = CS.AndConjunction(
                                            CS.GreaterThanCondition(maxpool_kernel_size_3, n_conv_layers, 2),
                                            CS.EqualsCondition(maxpool_kernel_size_3, maxpool_3, 'True')
        )
        use_maxpool_stride_3 = CS.AndConjunction(
                                            CS.GreaterThanCondition(maxpool_stride_3, n_conv_layers, 2),
                                            CS.EqualsCondition(maxpool_stride_3, maxpool_3, 'True')
        )
        config_space.add_conditions([use_kernel_3, use_stride_3, use_dropout_3, use_batch_norm_3, use_out_channel_3, use_maxpool_3, use_maxpool_kernel_3, use_maxpool_stride_3])

        dim_forbbiden_3 = CS.ForbiddenAndConjunction(
                                                    CS.ForbiddenEqualsClause(n_conv_layers, 3),
                                                    CS.ForbiddenInClause(maxpool_kernel_size_3, ['4', '5', '6'])
        )
        config_space.add_forbidden_clauses([dim_forbbiden_3])

        last_conv_forbidden_1 = CS.ForbiddenAndConjunction(
                                                        CS.ForbiddenEqualsClause(n_conv_layers, 3),
                                                        CS.ForbiddenEqualsClause(kernel_size_3, '7')
        )
        last_conv_forbidden_2 = CS.ForbiddenAndConjunction(
                                                        CS.ForbiddenEqualsClause(n_conv_layers, 2),
                                                        CS.ForbiddenEqualsClause(kernel_size_2, '7')
        )
        config_space.add_forbidden_clauses([last_conv_forbidden_1, last_conv_forbidden_2])

        ### Last fc layers ###
        n_out_1 = CSH.CategoricalHyperparameter('n_out_1', choices=['32', '64', '128', '256', '512'], default_value='512')
        n_out_2 = CSH.CategoricalHyperparameter('n_out_2', choices=['32', '64', '128', '256', '512'], default_value='128')
        n_out_3 = CSH.CategoricalHyperparameter('n_out_3', choices=['32', '64', '128', '256', '512'], default_value='32')
        config_space.add_hyperparameters([n_out_1, n_out_2, n_out_3])

        fc_forbidden_1 = CS.ForbiddenAndConjunction(
                                            CS.ForbiddenEqualsClause(n_out_1, '512'),
                                            CS.ForbiddenInClause(n_out_2, ['512'])
        )
        fc_forbidden_2 = CS.ForbiddenAndConjunction(
                                            CS.ForbiddenEqualsClause(n_out_1, '256'),
                                            CS.ForbiddenInClause(n_out_2, ['512', '256'])
        )
        fc_forbidden_3 = CS.ForbiddenAndConjunction(
                                            CS.ForbiddenEqualsClause(n_out_1, '128'),
                                            CS.ForbiddenInClause(n_out_2, ['512', '256', '128'])
        )
        fc_forbidden_4 = CS.ForbiddenAndConjunction(
                                            CS.ForbiddenEqualsClause(n_out_1, '64'),
                                            CS.ForbiddenInClause(n_out_2, ['512', '256', '128', '64']),
                                            CS.ForbiddenEqualsClause(n_layers, 3)
        )
        fc_forbidden_5 = CS.ForbiddenAndConjunction(
                                            CS.ForbiddenEqualsClause(n_out_1, '32'),
                                            CS.ForbiddenInClause(n_layers, [2,3])
        )
        fc_forbidden_6 = CS.ForbiddenAndConjunction(
                                            CS.ForbiddenEqualsClause(n_out_2, '256'),
                                            CS.ForbiddenInClause(n_out_3, ['512', '256'])
        )
        fc_forbidden_7 = CS.ForbiddenAndConjunction(
                                            CS.ForbiddenEqualsClause(n_out_2, '128'),
                                            CS.ForbiddenInClause(n_out_3, ['512', '256', '128'])
        )
        fc_forbidden_8 = CS.ForbiddenAndConjunction(
                                            CS.ForbiddenEqualsClause(n_out_2, '64'),
                                            CS.ForbiddenInClause(n_out_3, ['512', '256', '128', '64'])
        )
        fc_forbidden_9 = CS.ForbiddenAndConjunction(
                                            CS.ForbiddenEqualsClause(n_out_2, '32'),
                                            CS.ForbiddenEqualsClause(n_layers, 3)
        )
        # fc_forbidden_10 = CS.ForbiddenAndConjunction(
        #                                     CS.ForbiddenEqualsClause(n_layers, 1),
        #                                     CS.ForbiddenInClause(n_out_1, ['512', '256'])
        # )
        # fc_forbidden_11 = CS.ForbiddenAndConjunction(
        #                                     CS.ForbiddenEqualsClause(n_layers, 2),
        #                                     CS.ForbiddenInClause(n_out_1, ['512'])
        # )
        config_space.add_forbidden_clauses([fc_forbidden_1, fc_forbidden_2, fc_forbidden_3, fc_forbidden_4, fc_forbidden_5, fc_forbidden_6,
                                            fc_forbidden_7, fc_forbidden_8, fc_forbidden_9])

        use_fc_2 = CS.GreaterThanCondition(n_out_2, n_layers, 1)
        use_fc_3 = CS.GreaterThanCondition(n_out_3, n_layers, 2)
        config_space.add_conditions([use_fc_2, use_fc_3])

        return(config_space)



if __name__ == "__main__":
    cmdline_parser = argparse.ArgumentParser('AutoML SS19 final project')
    cmdline_parser.add_argument("-d", "--dataset",
                                default='KMNIST',
                                choices=['KMNIST', 'K49'],
                                help='Which dataset to use',
                                type=str.upper)
    cmdline_parser.add_argument("-s", "--surrogate_model",
                                default='RF',
                                choices=['KDE', 'RF', 'DNGO'],
                                help='kde, random forrest or dngo',
                                type=str.upper)
    cmdline_parser.add_argument('-i', "--n_iterations",
                                type=int,
                                default=1,
                                help='Number of BOHB iterations that will be run')
    cmdline_parser.add_argument('-b', "--min_budget",
                                type=int,
                                default=1,
                                help='Minimum BOHB budget')
    cmdline_parser.add_argument('-B', "--max_budget",
                                type=int,
                                default=2,
                                help='Maximum BOHB budget')
    cmdline_parser.add_argument('-e', "--eta",
                                type=int,
                                default=2,
                                help='The fraction of configurations passed to next budget by BOHB')
    cmdline_parser.add_argument('-o', "--out_dir",
                                type=str,
                                default='bohb/',
                                help='The output directory to write all results')
    cmdline_parser.add_argument('-r', "--run_id",
                                type=str,
                                default='bohb',
                                help='ID')

    cmdline_parser.add_argument('-v', '--verbose',
                                default='INFO',
                                choices=['INFO', 'DEBUG'],
                                help='verbosity')
    args, kwargs = cmdline_parser.parse_known_args()
    log_lvl = logging.INFO if args.verbose == 'INFO' else logging.DEBUG
    logging.basicConfig(level=log_lvl)

    start_time = time.time()

    dataset = args.dataset

    host = '127.0.0.1'
    NS = hpns.NameServer(run_id=args.run_id, host=host, port=0)
    ns_host, ns_port = NS.start()
    w = MyWorker(run_id=args.run_id, host=host, nameserver=ns_host, nameserver_port=ns_port, timeout=120)
    w.run(background=True)
    result_logger = hpres.json_result_logger(directory=args.out_dir, overwrite=True)

    if (args.surrogate_model == "KDE"):
        bohb = BOHB(  configspace = w.get_configspace(),
                      run_id = args.run_id,
                      host=host,
                      nameserver=ns_host,
                      nameserver_port=ns_port,
                      result_logger=result_logger,
                      min_budget=args.min_budget,
                      max_budget=args.max_budget,
                      eta = args.eta,
                      random_fraction=0.1,
                      num_samples=4)
    else:
        bohb = LCNet(    configspace = w.get_configspace(),
                         run_id = args.run_id,
                         eta=args.eta,
                         host=host,
                         nameserver=ns_host,
                         nameserver_port=ns_port,
                         result_logger=result_logger,
                         min_budget=args.min_budget,
                         max_budget=args.max_budget,
                         surrogate_model=args.surrogate_model)

    res = bohb.run(n_iterations=args.n_iterations)
    bohb.shutdown(shutdown_workers=True)
    NS.shutdown()

    print("took: ", time.time() - start_time)

    id2config = res.get_id2config_mapping()
    incumbent = res.get_incumbent_id()

    print('xxx' * 50)
    print('Best found configuration:', id2config[incumbent]['config'])
    print(res.get_runs_by_id(incumbent)[-1]['info'])
    print('xxx' * 50)
