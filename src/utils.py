import torch.nn as nn
import torch

class AvgrageMeter(object):

  def __init__(self):
    self.reset()

  def reset(self):
    self.avg = 0
    self.sum = 0
    self.cnt = 0

  def update(self, val, n=1):
    self.sum += val * n
    self.cnt += n
    self.avg = self.sum / self.cnt


def accuracy(output, target, topk=(1,)):
  maxk = max(topk)
  batch_size = target.size(0)

  _, pred = output.topk(maxk, 1, True, True)
  pred = pred.t()
  correct = pred.eq(target.view(1, -1).expand_as(pred))

  res = []
  for k in topk:
    correct_k = correct[:k].view(-1).float().sum(0)
    res.append(correct_k.mul_(100.0/batch_size))
  return res

data_dir = '../data'
data_augmentation = None

activation_dict =           {
                            'relu': nn.ReLU(inplace=False),
                            'sigmoid': nn.Sigmoid(),
                            'tanh': nn.Tanh()
                            }

architecture =              {
                            'n_layers': 3,
                            'n_conv_layers': 2,
                            'dropout': [True, True, False],
                            'dropout_rate': 0.2,
                            'batch_norm': [True, True, False],
                            'kernel_size': [2, 3],
                            'stride': [2, 2],
                            'padding': [1, 1],
                            'maxpool': [False, False, False],
                            'maxpool_kernel_size': [2, 2],
                            'maxpool_stride': [1, 1],
                            'out_channels': [4, 32],
                            'n_out': [256, 128, 64],
                            'activation': 'relu'
                            }

training_loss_dict =        {
                            'cross_entropy': torch.nn.CrossEntropyLoss,
                            'mse': torch.nn.MSELoss
                            }

optimizers_dict =           {
                            'adam': torch.optim.Adam,
                            'adad': torch.optim.Adadelta,
                            'sgd': torch.optim.SGD
                            }
optimizers_params_dict =    {
                            'adam': 'amsgrad',
                            'adad': None,
                            'sgd': 'momentum'
                            }

def translate_to_model_config(config):

    model_config = {}
    model_config['n_conv_layers'] = config['n_conv_layers']
    model_config['n_layers'] = config['n_layers']
    model_config['activation'] = config['activation']
    model_config['dropout_rate'] = float(config['dropout_rate'])

    model_config['kernel_size'] = []
    # model_config['padding'] = []
    model_config['stride'] = []
    model_config['out_channels'] = []
    model_config['batch_norm'] = []
    model_config['dropout'] = []
    model_config['maxpool'] = []
    model_config['maxpool_kernel_size'] = []
    model_config['maxpool_stride'] = []
    model_config['n_out'] = []

    for i in range(model_config['n_conv_layers']):
        model_config['kernel_size'].append(int(config['kernel_size_'+str(i+1)]))
        # model_config['padding'].append(int(config['padding_'+str(i+1)]))
        model_config['stride'].append(int(config['stride_'+str(i+1)]))
        model_config['out_channels'].append(int(config['out_channels_'+str(i+1)]))
        model_config['batch_norm'].append(eval(config['batch_norm_'+str(i+1)]))
        model_config['dropout'].append(eval(config['dropout_'+str(i+1)]))
        model_config['maxpool'].append(eval(config['maxpool_'+str(i+1)]))
        if model_config['maxpool'][i] is True:
          model_config['maxpool_kernel_size'].append(int(config['maxpool_kernel_size_'+str(i+1)]))
          model_config['maxpool_stride'].append(int(config['maxpool_stride_'+str(i+1)]))
        else:
          model_config['maxpool_kernel_size'].append(0)
          model_config['maxpool_stride'].append(0)
    for i in range(model_config['n_layers']):
        model_config['n_out'].append(int(config['n_out_'+str(i+1)]))

    return model_config


def translate_to_optimizer_config(config):

    optimizer_config = {}

    optimizer_config['model_optimizer'] = config['model_optimizer']
    optimizer_config['batch_size'] = int(config['batch_size'])
    optimizer_config['learning_rate'] = float(config['learning_rate'])
    optimizer_config['training_loss'] = config['training_loss']
    optimizer_config['optimizer_params'] = config['optimizer_params']
    optimizer_config['weight_decay'] = float(config['weight_decay'])

    return optimizer_config
