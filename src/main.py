import os
import argparse
import logging
import time
import numpy as np
import torch
import torchvision.transforms as transforms
from torch.utils.data import DataLoader
from torchsummary import summary

from customCNN import CustomCNN
from datasets import K49, KMNIST
from utils import *

def train(config,
         data_dir='../data',
         dataset='KMNIST',
         num_epochs=10,
         batch_size=50,
         learning_rate=0.001,
         weight_decay=0.01,
         train_criterion=torch.nn.CrossEntropyLoss,
         model_optimizer=torch.optim.Adam,
         optimizer_params=None,
         data_augmentations=None,
         save_model_str=None):
    """
    Training loop for configurableNet.
    :param model_config: network config (dict)
    :param data_dir: dataset path (str)
    :param dataset: dataset name (str)
    :param num_epochs: (int)
    :param batch_size: (int)
    :param learning_rate: model optimizer learning rate (float)
    :param train_criterion: Which loss to use during training (torch.nn._Loss)
    :param model_optimizer: Which model optimizer to use during trainnig (torch.optim.Optimizer)
    :param data_augmentations: List of data augmentations to apply such as rescaling.
        (list[transformations], transforms.Composition[list[transformations]], None)
        If none only ToTensor is used
    :return:
    """

    # Device configuration
    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

    if data_augmentations is None:
        # You can add any preprocessing/data augmentation you want here
        data_augmentations = transforms.ToTensor()
    elif isinstance(type(data_augmentations), list):
        data_augmentations = transforms.Compose(data_augmentations)
    elif not isinstance(data_augmentations, transforms.Compose):
        raise NotImplementedError

    if dataset == 'KMNIST':
        train_dataset = KMNIST(data_dir, True, data_augmentations)
        test_dataset = KMNIST(data_dir, False, data_augmentations)
    elif dataset == 'K49':
        train_dataset = K49(data_dir, True, data_augmentations)
        test_dataset = K49(data_dir, False, data_augmentations)
    else:
        raise NotImplementedError()

    # Make data batch iterable
    # Could modify the sampler to not uniformly random sample
    train_loader = DataLoader(dataset=train_dataset,
                              batch_size=batch_size,
                              shuffle=True)
    test_loader = DataLoader(dataset=test_dataset,
                             batch_size=batch_size,
                             shuffle=False)

    model = CustomCNN(config=config,
                       input_shape=(
                           train_dataset.channels,
                           train_dataset.img_rows,
                           train_dataset.img_cols
                       ),
                       num_classes=train_dataset.n_classes
            ).to(device)
    total_model_params = np.sum(p.numel() for p in model.parameters())
    # instantiate optimizer

    if model_optimizer == torch.optim.Adam:
        optimizer = model_optimizer(model.parameters(), lr=learning_rate, weight_decay=weight_decay, amsgrad=optimizer_params)
    elif model_optimizer == torch.optim.SGD:
        optimizer = model_optimizer(model.parameters(), lr=learning_rate, weight_decay=weight_decay, momentum=optimizer_params)
    else:
        optimizer = model_optimizer(model.parameters(), lr=learning_rate, weight_decay=weight_decay)

    # instantiate training criterion
    train_criterion = train_criterion().to(device)

    logging.info('Generated Network:')
    summary(model, (train_dataset.channels,
                    train_dataset.img_rows,
                    train_dataset.img_cols),
            device='cuda' if torch.cuda.is_available() else 'cpu')

    logging.info('Generated Optimizer:')
    logging.info(optimizer)
    learning_curve = []
    # Train the model
    for epoch in range(num_epochs):
        # logging.info('#' * 50)
        # logging.info('Epoch [{}/{}]'.format(epoch + 1, num_epochs))

        train_score, train_loss = model.train_fn(optimizer, train_criterion,
                                              train_loader, device)
        # logging.info('Train accuracy %f', train_score)

        test_score = model.eval_fn(test_loader, device)
        learning_curve.append(test_score)
        # logging.info('Test accuracy %f', test_score)
    # logging.info('Train accuracy %f', train_score) ####
    # logging.info('Test accuracy %f', test_score) ####
    # if save_model_str:
    #     # Save the model checkpoint can be restored via "model = torch.load(save_model_str)"
    #     if os.path.exists(save_model_str):
    #         save_model_str += dataset
    #         save_model_str += '_'.join(time.ctime())
    #     torch.save(model.state_dict(), save_model_str)

    return train_score, train_loss, test_score, learning_curve
