import numpy as np
import torch
import torch.nn.functional as F
import torch.nn as nn
from torch.autograd import Variable
from tqdm import tqdm
import logging


from utils import *


class CustomCNN(nn.Module):
    def __init__(self, config, input_shape=(1, 28, 28), num_classes=10, config_from_bohb=True):
        """
        Construtor method
        :param model_config:
        :input_shape:
        :num_classes:
        :return:
        """

        super(CustomCNN, self).__init__()

        layers = []

        model_config=translate_to_model_config(config) if config_from_bohb else config
        self.model_config = model_config

        n_layers = model_config['n_layers']
        n_conv_layers = model_config['n_conv_layers']
        dropout = model_config['dropout']
        dropout_rate = model_config['dropout_rate']
        batch_norm = model_config['batch_norm']
        activation = model_config['activation']
        kernel_size = model_config['kernel_size']
        stride = model_config['stride']
        # padding = model_config['padding']
        out_channels = model_config['out_channels']
        maxpool = model_config['maxpool']
        maxpool_kernel_size = model_config['maxpool_kernel_size']
        maxpool_stride = model_config['maxpool_stride']
        n_out = model_config['n_out']


        in_channels = input_shape[0]
        out_size = input_shape

        for n_layer in range(n_conv_layers):

            padding, out_size = self._get_same_padding(out_size, layers, stride[n_layer], kernel_size[n_layer], in_channels)

            c = nn.Conv2d(in_channels, out_channels[n_layer],
                            kernel_size=kernel_size[n_layer],
                            stride=stride[n_layer],
                            padding=padding
                            )
            layers.append(c)

            if batch_norm[n_layer] is True:
                    b = nn.BatchNorm2d(out_channels[n_layer])
                    layers.append(b)

            a = activation_dict[activation]
            layers.append(a)

            if dropout[n_layer] is True:
                    d = nn.Dropout(dropout_rate)
                    layers.append(d)

            if maxpool[n_layer] is True:
                p = nn.MaxPool2d(kernel_size=maxpool_kernel_size[n_layer],
                                   stride=maxpool_stride[n_layer])
                layers.append(p)
            in_channels = out_channels[n_layer]

        self.conv_layers = nn.Sequential(*layers)
        self.output_size = num_classes

        self.fc_layers = nn.ModuleList()
        n_in = self._get_conv_output(input_shape)
        for n_layer in range(n_layers):
            fc = nn.Linear(n_in, n_out[n_layer])
            self.fc_layers += [fc]
            n_in = n_out[n_layer]

        self.last_fc = nn.Linear(int(n_in), self.output_size)
        self.dropout = nn.Dropout(p=0.2)

    # generate input sample and forward to get shape
    def _get_conv_output(self, shape):
        bs = 1
        input = Variable(torch.rand(bs, *shape))
        output_feat = self.conv_layers(input)
        n_size = output_feat.data.view(bs, -1).size(1)
        return n_size

    def _get_same_padding(self, out_size, layers, stride, kernel_size, in_channels):

        bs = 1
        input = Variable(torch.rand(bs, *out_size))
        try:
            output_feat = layers(input)
            n_size = output_feat.size()[1]
        except:
            n_size = out_size[1]

        pad = int(((n_size - 1)*stride - n_size + kernel_size)/2)
        return pad, (in_channels, n_size, n_size)

    def forward(self, x):
        x = self.conv_layers(x)
        x = x.view(x.size(0), -1)
        for fc_layer in self.fc_layers:
            x = self.dropout(F.relu(fc_layer(x)))
        x = self.last_fc(x)
        return x

    def train_fn(self, optimizer, criterion, loader, device, train=True):
        """
        Training method
        :param optimizer: optimization algorithm
        :criterion: loss function
        :param loader: data loader for either training or testing set
        :param device: torch device
        :param train: boolean to indicate if training or test set is used
        :return: (accuracy, loss) on the data
        """
        score = AvgrageMeter()
        objs = AvgrageMeter()
        self.train()

        t = tqdm(loader)
        for images, labels in t:
            images = images.to(device)
            labels = labels.to(device)
            logits = self(images)

            loss = criterion(logits, labels)
            optimizer.zero_grad()
            logits = self(images)
            loss = criterion(logits, labels)
            loss.backward()
            optimizer.step()

            acc, _ = accuracy(logits, labels, topk=(1, 5))
            n = images.size(0)
            objs.update(loss.item(), n)
            score.update(acc.item(), n)

            # t.set_description('(=> Training) Loss: {:.4f}'.format(objs.avg))

        return score.avg, objs.avg

    def eval_fn(self, loader, device, train=False):
        """
        Evaluation method
        :param loader: data loader for either training or testing set
        :param device: torch device
        :param train: boolean to indicate if training or test set is used
        :return: accuracy on the data
        """
        score = AvgrageMeter()
        self.eval()

        t = tqdm(loader)
        with torch.no_grad():  # no gradient needed
            for images, labels in t:
                images = images.to(device)
                labels = labels.to(device)

                outputs = self(images)
                acc, _ = accuracy(outputs, labels, topk=(1, 5))
                score.update(acc.item(), images.size(0))

                # t.set_description('(=> Test) Score: {:.4f}'.format(score.avg))

        return score.avg
