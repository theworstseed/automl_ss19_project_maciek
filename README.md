### Due: 20.09.19 (23:59 CET)

# AutoML_SS19_project

Presentation.pdf 		- final presentation
src/my_bohb.py 			- BOHB for parallel NAS and optimizer optimization
src/cnn_darts/train_search.py	- DARTS search
src/cnn_darts/my_bohb.py	- BOHB for optimizer and some architecture hyperparameters with resulted genotype
src/cnn_darts/test.py		- train and validation on the final model
